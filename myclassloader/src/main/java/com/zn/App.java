package com.zn;

import java.sql.SQLOutput;

/**
 * @Description:
 * @Author: zhangnan
 * @Date: 2019/11/30
 */
public class App {

    public static void main(String[] args) {
        MyClassLoader loader = new MyClassLoader("myloader","C:\\Users\\zhangnan\\Desktop\\");
        try {
            //测试时，直接删除编译后的Dog.class，并将其复制到其他目录， 则能明白什么是双亲委派机制
            Class<?> aClass = loader.loadClass("com.zn.Dog");
            Object dog = aClass.newInstance();
            System.out.println(dog);
            System.out.println(dog.getClass().getClassLoader());
            System.out.println(System.getProperty("sun.boot.class.path"));
            System.out.println(System.getProperty("java.ext.dirs"));
            System.out.println(System.getProperty("java.class.path"));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
