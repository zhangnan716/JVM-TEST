/**
 * @Description:
 * @Author: zhangnan
 * @Date: 2019/11/30
 *
 * 理解初始会的过程0
 */
public class TestStatic2 {

    public static void main(String[] args) {
        Singleton instance = Singleton.getInstance();
        System.out.println(Singleton.counter1);
        System.out.println(Singleton.counter2);
    }

}

class Singleton{

    public static int counter1 = 1;
    private static Singleton singleton = new Singleton();

    private Singleton(){
        counter1++;
        counter2++;
    }

    public static int counter2 = 1;

    public static Singleton getInstance(){
        return singleton;
    }

}
