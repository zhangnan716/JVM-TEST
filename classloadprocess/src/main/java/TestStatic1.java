/**
 * @Description:
 * @Author: zhangnan
 * @Date: 2019/11/30
 *一、生命周期
 * -加载：查找加载类的二进制数据
 * -连接：
 *    -- 验证二进制文件的正确性
 *    -- 准备  （*****）
 *       为类的静态变量分配内存，并将其初始化为默认值
 *    -- 解析  把类中的符号引用转化为直接引用
 * -初始化： 把类的静态变量赋予正确的初始值
 *    -- 没有连接 则先加载和连接
 *    -- 存在父类 先初始化直接父类
 *    -- 有初始化语句 按照顺序执行
 * -卸载
 *    --由系统的类加载器加载的类 始终不会被卸载
 *    --自定义实现的可以卸载，将类加载器置为null，调用System.gc()即可
 * -类实例化：
 *    -- 为新的对象分配内存
 *    -- 为实例变量赋予默认值
 *    --为实例变量赋予正确的初始值
 * -垃圾回收 对象销毁
 *
 *
 * 对于数组而言，其类型是JVM在运行期间动态生成的，没有对应的classloader，他的class就是数组成员的类型
 *
 * 助记码
 *   anewarray  创建一个引用类型的数据并将其压入栈顶
 *   newarray  创建一个原始类型的数据并将其压入栈顶
 *
 *
 * 1、类会在第一次主动使用时加载，如果class文件缺失会报LinkageError错误
 *    （
 *      1、创建类的实例
 *      2、访问某个类或接口的静态变量，或对静态变量赋值
 *      3、调用类的静态方法
 *      4、反射
 *      5、初始化一个类的子类
 *      6、虚拟机启动时被标明为启动类的类
 *
 *    ）
 *
 * 1、重点一：static final 修饰的常量会在编译时，进入使用者（TestStatic1）的class文件中的常量池中，
 *          调用时会直接使用本class文件的常量池，编译之后就和其他的类无关了。
 * 2、一个类初始化时一定会初始化其父类，而接口初始化时不要求初始化其父类。
 * 3、接口的常量 默认是public static final的
 *
 */


public class TestStatic1 {

    public static void main(String[] args) {
        System.out.println(Child.d);
    }

}

class Parent{

    public static int a = 1;

    public static final int b = 2;

    static {
        System.out.println("parent static block！;");
    }

}

class Child extends Parent{

    public static int c = 3;
    public static final int d = 4;

    static {
        System.out.println("child static block;");
    }

}
